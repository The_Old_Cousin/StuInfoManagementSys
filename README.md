# 北软毕设管理系统

#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是开源中国推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

这是项目页面，整个跟团队项目有关的项工程文件全部都在这里。

**想必许多老师和学生在进行校内信息搜集时都会有一定的困惑，而这个平台可以:**

    ·帮助学生更为快速直观的填写，查看和修改个人所需信息
    ·帮助教师实时读写和查看学生信息
    ·减少纸张的过度浪费
    ·统一化管理加快行政效率
    ·在校园安全防火墙系统的管理下，减少信息的泄露，加大了对信息安全的保障
    ·拥有用户历史记录信息,用户查信息一段时间内需要对该信息反复查询,因此历史记录可以帮助用户节约重复查询检索和修改的时间


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)